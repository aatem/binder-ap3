# Jupyter environment for M1 mathinfo

## Try it on Binder

Based on [repo2docker](https://repo2docker.readthedocs.io/en/latest/usage.html).
Use it on [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.univ-paris-diderot.fr%2Fmolin%2Fbinder-ap3.git/HEAD)
